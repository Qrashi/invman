package link.ignyte.plugins.invman.item.lore;

import link.ignyte.plugins.invman.item.Item;
import link.ignyte.plugins.invman.item.Lore;
import org.bukkit.Material;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

/**
 * Tests to check the validity of this implementation of a Lore serializer
 */
public class TestDefaultLayoutSerializer {

    /**
     * Convert an array of strings into an '\n' seperated string
     * @param list list to convert
     * @return the string (I know there are better options leave me alone)
     */
    private static String arrayToString(ArrayList<String> list) {
        StringBuilder result = new StringBuilder();
        for (String item : list) {
            result.append(item).append("\n");
        }
        return result.substring(0, result.length()-1);
    }

    /**
     * Test what happens if the lore is completely empty
     */
    @Test
    public void testEmptyLore() {
        Assertions.assertEquals(0, new DefaultLayoutSerializer().serialize(new Lore(new Item(Material.AIR))).size());
    }

    /**
     * Test the line breaks if the description doesn't fit one line
     */
    @Test
    public void testDescriptionLineBreaks() {
        Assertions.assertEquals(
                "&f&btest test:\n &7- this is a description, it is very interesting and also lo\n &7  nger than the max character limit\n &7  and has a break\n\n &a+ &7do something\n\n&8testFooter",
                arrayToString(new DefaultLayoutSerializer().serialize(new Lore(new Item(Material.AIR))
                        .setTitle("test test")
                        .addLine("this is a description, it is very interesting and also longer than the max character limit\nand has a break")
                        .setFooter("testFooter")
                        .setAction_left("do something"))));
    }

    /**
     * Test if both actions get put into one line if small enough and get seperated if too big
     */
    @Test
    public void testBothActionsOneLine() {
        Assertions.assertEquals(
                "&f&btest:\n &7- testdesc, not complex\n\n &a+ &7do something &f| &c- &7do nothing",
                arrayToString(new DefaultLayoutSerializer().serialize(new Lore(new Item(Material.AIR))
                        .setTitle("test")
                        .addLine("testdesc, not complex")
                        .setAction_left("do something")
                        .setAction_right("do nothing")
                )));
        Assertions.assertEquals(
                "&f&btest:\n &7- testdesc, not complex\n\n &a+ &7do something that is very complex and requires more than one line\n &c- &7do nothing",
                arrayToString(new DefaultLayoutSerializer().serialize(new Lore(new Item(Material.AIR))
                        .setTitle("test")
                        .addLine("testdesc, not complex")
                        .setAction_left("do something that is very complex and requires more than one line")
                        .setAction_right("do nothing")
                )));
    }

    /**
     * Test what happens when the only thing set is the drop action
     */
    @Test
    public void testOnlyDropAndFooter() {
        Assertions.assertEquals(
                " &bQ &7interesting",
                arrayToString(new DefaultLayoutSerializer().serialize(new Lore(new Item(Material.AIR))
                        .setAction_drop("interesting")
                )));
        Assertions.assertEquals(
                "&8interesting",
                arrayToString(new DefaultLayoutSerializer().serialize(new Lore(new Item(Material.AIR))
                        .setFooter("interesting")
                )));
    }
}
