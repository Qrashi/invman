package link.ignyte.plugins.invman;

import link.ignyte.plugins.invman.item.Item;
import link.ignyte.plugins.invman.item.listeners.ListenerStatistics;
import link.ignyte.utils.JsonSingleton.GsonMode;
import link.ignyte.utils.JsonSingleton.JsonSingletons;
import org.bukkit.Material;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;

/**
 * Testing class for InvMan (stats serialisation)
 */
public class TestInvman {

    /**
     * Test registering an uninitialized item
     */
    @Test
    public void testRegisterUninitialized() {
        Assertions.assertThrows(NotInitializedException.class, () -> {
            Item item = new Item(Material.AIR);
            item.register();
        });
    }

    /**
     * Test editing an unregistered click-action
     */
    @Test
    public void testUnregisteredClickAction() {
        Assertions.assertThrows(NotInitializedException.class, () -> new Item(Material.AIR).editClick((event) -> event.player.sendMessage("won't work")));
    }

    /**
     * Test if the creation of a temporary directory works.
     * @param dir the directory to save to
     */
    @Test
    public void testTempDir(@TempDir File dir) {
        Assertions.assertNotNull(dir);
        Assertions.assertTrue(dir.exists());
    }

    /**
     * Test saving some random statistics
     * @param tempDir the directory to save to
     */
    @Test
    public void testStatisticsLoadFromDefault(@TempDir File tempDir) {
        JsonSingletons.initialize(GsonMode.ALL, tempDir.getAbsolutePath());
        ListenerStatistics.save(ListenerStatistics.class);
        // Will save the defaults
        Assertions.assertFalse(new File(tempDir.getAbsolutePath() + "/stats.json").exists());
        // Assert if file doesn't exist
    }
}

