package link.ignyte.plugins.invman.inventories;

import link.ignyte.plugins.invman.Invman;
import link.ignyte.plugins.invman.item.Item;
import link.ignyte.plugins.invman.item.ItemManager;
import link.ignyte.plugins.invman.utils.Color;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * A managed inventory provides an easy to use api to change your items. Don't lose it, but rather
 * change it using the change method (will give you access to the Item API)
 * Will be updated automatically (if required)
 */
public class ManagedInventory {
    Inventory inventory;

    /**
     * Create a new managed inventory
     * @param title title of inventory
     */
    public ManagedInventory(String title) {
        create(45, title, Invman.filler.itemStack);
    }

    /**
     * Create a new managed inventory
     * @param size size of inventory (must be dividable by 9)
     * @param title title of inventory
     */
    public ManagedInventory(int size, String title) {
        create(size, title, Invman.filler.itemStack);
    }

    /**
     * Create a new managed inventory
     * @param type type of inventory
     * @param title title of inventory
     */
    public ManagedInventory(InventoryType type, String title) {
        create(type, title, Invman.filler.itemStack);
    }

    /**
     * Create an inventory from size
     * @param size inventory size
     * @param title inventory title
     * @param filler filler item to fill inventory with
     */
    private void create(int size, String title, ItemStack filler) {
        inventory = Bukkit.createInventory(null, size, Color.translate(title));
        for (int index = 0; index < size; index++) {
            inventory.setItem(index, filler);
        }
    }

    /**
     * Creater an inventory from type
     * @param type inventory type
     * @param title inventory title
     * @param filler filler item to fill inventory with
     */
    private void create(InventoryType type, String title, ItemStack filler) {
        inventory = Bukkit.createInventory(null, type, Color.translate(title));
        for (int index = 0; index < inventory.getSize(); index++) {
            inventory.setItem(index, filler);
        }
    }

    /**
     * Will return the item at index if there is an item there
     * if defaulted, will create new item
     * @param index the index of the item
     * @return self (singleton pattern)
     */
    public Item change(int index) {
        ItemStack itemStack = inventory.getItem(index);
        if (itemStack != null) {
            Item item = ItemManager.get(itemStack);
            if (item != Invman.filler) {
                return item;
            }
        } else {
            // No item there (clear), create one
            itemStack = new ItemStack(Invman.filler.itemStack.getType());
            inventory.setItem(index, itemStack);
        }
        return new Item(itemStack);
    }

    /**
     * Set slot to nothing (AIR)
     * @param index slot to set
     * @return self (singleton pattern)
     */
    public ManagedInventory clear(int index) {
        inventory.setItem(index, new ItemStack(Material.AIR));
        return this;
    }

    /**
     * Set a slot to default state
     * @param index slot
     * @return self (singleton pattern)
     */
    public ManagedInventory setToDefault(int index) {
        inventory.setItem(index, Invman.filler.itemStack);
        return this;
    }

    /**
     * Open the inventory to a player with a 1 tick delay
     * @param player player
     * @return self (singleton pattern)
     */
    public ManagedInventory open(Player player) {
        new BukkitRunnable() {
            @Override
            public void run() {
                player.openInventory(inventory);
            }
        }.runTaskLater(Invman.getPlugin(), 1);
        return this;
    }
}
