package link.ignyte.plugins.invman.utils;

import link.ignyte.plugins.invman.Invman;
import org.bukkit.NamespacedKey;

/**
 * Helps with the library namespace, generates NamespacedKeys
 */
public class Namespace {
    /**
     * Generate a namespaced key
     * @param key key
     * @return the namespaced key
     */
    public static NamespacedKey getKey(String key) {
        return new NamespacedKey(Invman.getPlugin(), "invman/" + key);
    }
}
