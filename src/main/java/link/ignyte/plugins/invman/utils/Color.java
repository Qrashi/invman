package link.ignyte.plugins.invman.utils;

import org.bukkit.ChatColor;

import java.util.ArrayList;

public class Color {

    /**
     * Colorize a string with color markers ('&') into real text
     * @param text text to colorize
     * @return the colored text
     */
    public static String translate(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    /**
     * Colorize a list of color marked texts
     * @param text test (list) to colorize
     * @return the colorized list
     */
    public static ArrayList<String> translate(ArrayList<String> text) {
        ArrayList<String> result = new ArrayList<>();
        text.forEach((item) -> result.add(translate(item)));
        return result;
    }
}
