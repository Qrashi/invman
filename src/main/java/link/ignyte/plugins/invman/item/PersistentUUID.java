package link.ignyte.plugins.invman.item;

import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * A Container type to store UUIDs in in-game items (stores UUIDs as bytes)
 */
public class PersistentUUID implements PersistentDataType<byte[], UUID> {
    /**
     * Get the primitive storage type of this container
     * @return primitive type
     */
    public @NotNull Class<byte[]> getPrimitiveType() {
        return byte[].class;
    }

    /**
     * Get the complex type (UUIDs)
     * @return the complex type
     */
    public @NotNull Class<UUID> getComplexType() {
        return UUID.class;
    }

    /**
     * Convert a complex object to a primitive object (UUID to byte)
     * @param complex the complex object instance
     * @param context the context this operation is running in
     * @return primitive (bytes)
     */
    public byte @NotNull [] toPrimitive(UUID complex, @NotNull PersistentDataAdapterContext context) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(complex.getMostSignificantBits());
        bb.putLong(complex.getLeastSignificantBits());
        return bb.array();
    }

    /**
     * Get the UUID from bytes
     * @param primitive the primitive value
     * @param context the context this operation is running in
     * @return complex (UUID)
     */
    public @NotNull UUID fromPrimitive(byte @NotNull [] primitive, @NotNull PersistentDataAdapterContext context) {
        ByteBuffer bb = ByteBuffer.wrap(primitive);
        long firstLong = bb.getLong();
        long secondLong = bb.getLong();
        return new UUID(firstLong, secondLong);
    }
}