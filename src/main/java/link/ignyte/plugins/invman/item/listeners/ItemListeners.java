package link.ignyte.plugins.invman.item.listeners;

import link.ignyte.plugins.invman.Invman;
import link.ignyte.plugins.invman.item.Item;
import link.ignyte.plugins.invman.item.ItemManager;
import link.ignyte.plugins.invman.item.PersistentUUID;
import link.ignyte.plugins.invman.item.event.ClickType;
import link.ignyte.plugins.invman.item.event.ItemEvent;
import link.ignyte.plugins.invman.utils.Namespace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;

import java.util.UUID;

/**
 * Listener classes to manage in-game events
 */
public class ItemListeners implements Listener {

    private static final ListenerStatistics statistics = ListenerStatistics.get(ListenerStatistics.class);


    /**
     * Listens to click events and calls the corresponding functions if a player clicked a registered item
     * @param event event data
     */
    @EventHandler
    public void onClick(InventoryClickEvent event) {
        statistics.clicks++;
        ItemStack itemStack = event.getCurrentItem();
        if (itemStack != null) {
            if (itemStack.hasItemMeta()) {
                ItemMeta meta = itemStack.getItemMeta();
                if (meta != null) {
                    PersistentDataContainer container = meta.getPersistentDataContainer();
                    if (container.has(Namespace.getKey("instance"), new PersistentUUID())) {
                        if (container.get(Namespace.getKey("instance"), new PersistentUUID()) == Invman.getInstanceUUID()) {
                            // parse this item, registered item in our instance
                            UUID uuid = container.get(Namespace.getKey("uuid"), new PersistentUUID());
                            Item item = ItemManager.getFromUUUID(uuid);
                            ItemEvent itemEvent = new ItemEvent(ClickType.LEFT_CLICK, (Player) event.getWhoClicked(), item, event.isShiftClick());
                            if (event.isRightClick()) {
                                itemEvent.clickType = ClickType.RIGHT_CLICK;
                            } if (event.getClick().isCreativeAction()) {
                                itemEvent.clickType = ClickType.MIDDLE_CLICK;
                            }
                            item.run(itemEvent);
                            event.setCancelled(itemEvent.cancel);
                            statistics.executed++;
                        }
                    }
                }
            }
        }
    }

    /**
     * Listens to drag events and calls the corresponding functions if a player has dragged a registered item
     * @param event event data
     */
    @EventHandler
    public void onDrag(InventoryDragEvent event) {
        statistics.clicks++;
        ItemStack itemStack = event.getCursor();
        if (itemStack != null) {
            if (itemStack.hasItemMeta()) {
                ItemMeta meta = itemStack.getItemMeta();
                if (meta != null) {
                    PersistentDataContainer container = meta.getPersistentDataContainer();
                    if (container.has(Namespace.getKey("instance"), new PersistentUUID())) {
                        if (container.get(Namespace.getKey("instance"), new PersistentUUID()) == Invman.getInstanceUUID()) {
                            // parse this item, registered item in our instance
                            UUID uuid = container.get(Namespace.getKey("uuid"), new PersistentUUID());
                            Item item = ItemManager.getFromUUUID(uuid);
                            ItemEvent itemEvent = new ItemEvent(ClickType.LEFT_CLICK, (Player) event.getWhoClicked(), item, false);
                            item.run(itemEvent);
                            event.setCancelled(itemEvent.cancel);
                            statistics.executed++;
                        }
                    }
                }
            }
        }
    }

    /**
     * Listens to interact events and calls the corresponding functions if a player has interacted with a registered item
     * @param event event data
     */
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        statistics.clicks++;
        ItemStack itemStack = event.getItem();
        if (itemStack != null) {
            if (itemStack.hasItemMeta()) {
                ItemMeta meta = itemStack.getItemMeta();
                if (meta != null) {
                    PersistentDataContainer container = meta.getPersistentDataContainer();
                    if (container.has(Namespace.getKey("instance"), new PersistentUUID())) {
                        if (container.get(Namespace.getKey("instance"), new PersistentUUID()) == Invman.getInstanceUUID()) {
                            // parse this item, registered item in our instance
                            UUID uuid = container.get(Namespace.getKey("uuid"), new PersistentUUID());
                            Item item = ItemManager.getFromUUUID(uuid);
                            ItemEvent itemEvent = new ItemEvent(ClickType.LEFT_CLICK, event.getPlayer(), item, event.getPlayer().isSneaking());
                            if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                                itemEvent.clickType = ClickType.RIGHT_CLICK;
                                item.run(itemEvent);
                                event.setCancelled(itemEvent.cancel);
                                statistics.executed++;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Listens to drop events and calls the corresponding functions if a registered item has been dropped
     * @param event event data
     */
    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        statistics.drops++;
        ItemStack itemStack = event.getItemDrop().getItemStack();
        if (itemStack.hasItemMeta()) {
            ItemMeta meta = itemStack.getItemMeta();
            if (meta != null) {
                PersistentDataContainer container = meta.getPersistentDataContainer();
                if (container.has(Namespace.getKey("instance"), new PersistentUUID())) {
                    if (container.get(Namespace.getKey("instance"), new PersistentUUID()) == Invman.getInstanceUUID()) {
                        // parse this item, registered item in our instance
                        UUID uuid = container.get(Namespace.getKey("uuid"), new PersistentUUID());
                        Item item = ItemManager.getFromUUUID(uuid);
                        ItemEvent itemEvent = new ItemEvent(ClickType.DROP, event.getPlayer(), item, event.getPlayer().isSneaking());
                        item.run(itemEvent);
                        event.setCancelled(itemEvent.cancel);
                        statistics.executed++;
                    }
                }
            }
        }
    }
}
