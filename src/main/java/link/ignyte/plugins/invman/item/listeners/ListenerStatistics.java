package link.ignyte.plugins.invman.item.listeners;

import link.ignyte.utils.JsonSingleton.JsonPath;
import link.ignyte.utils.JsonSingleton.JsonSerializable;

/**
 * A statistics object (singleton)
 */
@JsonPath(value = "stats.json")
public class ListenerStatistics extends JsonSerializable {

    /**
     * Generate a new statistics object
     */
    public ListenerStatistics() {
        clicks = 0;
        drops = 0;
        executed = 0;
    }

    int clicks;
    int drops;
    int executed;
}
