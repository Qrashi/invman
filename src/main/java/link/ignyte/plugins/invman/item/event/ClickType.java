package link.ignyte.plugins.invman.item.event;

/**
 * Types of click from user (only important ones)
 */
public enum ClickType {
    LEFT_CLICK,
    RIGHT_CLICK,
    DROP,
    MIDDLE_CLICK,
}
