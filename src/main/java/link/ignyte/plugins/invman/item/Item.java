package link.ignyte.plugins.invman.item;

import link.ignyte.plugins.invman.Invman;
import link.ignyte.plugins.invman.item.event.ItemEvent;
import link.ignyte.plugins.invman.utils.Namespace;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;

import java.util.UUID;
import java.util.function.Consumer;

import static link.ignyte.plugins.invman.utils.Color.translate;

/**
 * An in-game item wrapped in easy to use APIs
 */
public class Item {
    /*
    An Item (utility class to change an itemStacks properties)
     */
    public ItemStack itemStack;
    protected ItemMeta meta;
    InteractionManager interactionManager = null;
    private final Lore lore;
    boolean metaChanged = false;
    boolean registered = false;

    /**
     * Initialize an Item from an already existing item
     * @param item original in-game item stack
     */
    public Item(ItemStack item) {
        // Initialisation from existing itemstack - NOT registered!
        this.itemStack = item;
        meta = this.itemStack.getItemMeta();
        if (meta == null) {
            meta = Bukkit.getItemFactory().getItemMeta(item.getType());
            lore = new Lore(this);
        } else {
            lore = new Lore(this, meta.getLore());
        }
    }

    /**
     * Create a new item from scratch
     * @param item material to use
     */
    public Item(Material item) {
        // Initialisation from scratch - also NOT registered
        if (item != Material.AIR) {
            this.itemStack = new ItemStack(item);
            meta = this.itemStack.getItemMeta();
        }
        lore = new Lore(this);
    }

    /**
     * Run the desired action
     */
    public void run(ItemEvent event) {
        interactionManager.runnable.accept(event);
    }

    /**
     * Make the item do nothing on its own but be unmovable
     * @return return self (singleton pattern)
     */
    public Item makeUnmoveable() {
        register();
        return this;
    }

    /**
     * Set the material of the item
     * @param material new material
     * @return return self (singleton pattern)
     */
    public Item setMaterial(Material material) {
        itemStack.setType(material);
        return this;
    }

    /**
     * Set the amount of the item
     * @param amount new amount
     * @return return self (singleton pattern)
     */
    public Item setAmount(int amount) {
        itemStack.setAmount(amount);
        return this;
    }

    /**
     * Set the displayName of the Item
     * @param name the new display name
     * @return return self (singleton pattern)
     */
    public Item setName(String name) {
        metaChanged = true;
        meta.setDisplayName(translate(name));
        return this;
    }

    /**
     * Edit the lore
     * @return the lore object of the item
     */
    public Lore editLore() {
        lore.updated = false;
        return lore;
    }

    /**
     * Edit the function that will be run on click
     * @return return self (singleton pattern)
     */
    public Item editClick(Consumer<ItemEvent> onClick) {
        Invman.throwUninitialized("cannot set onClick action with uninitialized InvMan!");
        if (!registered) {
            register();
            interactionManager = new InteractionManager();
        }
        interactionManager.runnable = onClick;
        return this;
    }

    /**
     * Register this item to the database
     * @return return self (singleton pattern)
     */
    public Item register() {
        Invman.throwUninitialized("error while registering Item: InvMan not initialized");
        if (registered) { return this; }
        UUID uuid = ItemManager.register(this);
        PersistentDataContainer container = meta.getPersistentDataContainer();
        container.set(Namespace.getKey("uuid"), new PersistentUUID(), uuid);
        container.set(Namespace.getKey("instance"), new PersistentUUID(), Invman.getInstanceUUID());
        metaChanged = true;
        registered = true;
        return this;
    }

    /**
     * Apply changes to the in-game item
     * @return self (singleton pattern)
     */
    public Item apply() {
        if (metaChanged) {
            itemStack.setItemMeta(meta);
        }
        metaChanged = false;
        return this;
    }
}
