package link.ignyte.plugins.invman.item;

import link.ignyte.plugins.invman.Invman;
import link.ignyte.plugins.invman.utils.Namespace;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;

import java.util.HashMap;
import java.util.UUID;

/**
 * ItemManager, the class holding all items and corresponding UUIDs
 */
public class ItemManager {
    private static final HashMap<UUID, Item> items = new HashMap<>();

    /**
     * Register a new Item
     * @param item Item to register
     * @return UUID of new Item
     */
    public static UUID register(Item item) {
        UUID uuid = UUID.randomUUID();
        items.put(uuid, item);
        return uuid;
    }

    /**
     * Get the corresponding Item from an ItemStack or create an Item if not an Item
     * @param item in-game ItemStack
     * @return valid, editable, Item
     */
    public static Item get(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        if (meta != null) {
            PersistentDataContainer container = meta.getPersistentDataContainer();
            if (container.has(Namespace.getKey("instance"), new PersistentUUID())) {
                if (container.get(Namespace.getKey("instance"), new PersistentUUID()) == Invman.getInstanceUUID()) {
                    // check if same instance (could be outdated instance or instance of other plugin)
                    UUID uuid = container.get(Namespace.getKey("uuid"), new PersistentUUID());
                    // "uuid" will always be present if "instance" is
                    if (items.containsKey(uuid)) {
                        return items.get(uuid);
                    }
                }
            }
        }
        return new Item(item);
    }

    /**
     * Get an item from its corresponding UUID
     * @param uuid UUID of item
     * @return corresponding Item (completely random stone item)
     */
    public static Item getFromUUUID(UUID uuid) {
        if (items.containsKey(uuid)) {
            return items.get(uuid);
        }
        return new Item(Material.STONE); // Shouldn't happen
    }
}
