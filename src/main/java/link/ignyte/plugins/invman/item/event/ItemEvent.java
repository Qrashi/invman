package link.ignyte.plugins.invman.item.event;

import link.ignyte.plugins.invman.item.Item;
import org.bukkit.entity.Player;

/**
 * Holds all information about the current item event
 */
public class ItemEvent {
    public ClickType clickType;
    public Player player;
    public Item item;
    public boolean shift;
    public boolean cancel;

    /**
     * Create a new ItemEvent
     * @param clickType type of click that occurred
     * @param player player that clicked
     * @param item item that was used (Item API)
     * @param shift Weather or not the shift key has been pressed
     */
    public ItemEvent(ClickType clickType, Player player, Item item, boolean shift) {
        this.clickType = clickType;
        this.player = player;
        this.item = item;
        this.shift = shift;
    }

    /**
     * Set the cancellation state of this event
     * @param cancel new cancellation state
     */
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }
}
