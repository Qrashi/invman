package link.ignyte.plugins.invman.item.lore;

import link.ignyte.plugins.invman.item.Lore;

import java.util.ArrayList;

/**
 * The standard Lore serializer from the library
 */
public class DefaultLayoutSerializer implements LoreSerializer {

    /*
    Layout for this implementation:

    &fTitle
 (&7) - description[n]
 (&7) - description[n]
        continue text if \n found

    &a+ left info (&c- info; if short enough)
    (&c- left info; if too long)
    &dM middle click info if present

    footer
     */

    public static final int MAX_WIDTH_CHAR = 60;

    /**
     * Serialize a Lore object to a lore description text
     * @param lore lore object
     * @return the description text, one line is one element in the list
     */
    @Override
    public ArrayList<String> serialize(Lore lore) {
        ArrayList<String> result = new ArrayList<>();
        if (lore.getTitle() == null) {
            if (lore.getDescription().size() != 0) {
                result.add("&f&bInfo:");
            }
        } else {
            if (lore.getTitle().endsWith(":")) {
                result.add("&f&b" + lore.getTitle());
            } else {
                result.add("&f&b" + lore.getTitle() + ":");
            }
        }
        for (String item : lore.getDescription()) {
            String current_line = " &7- ";
            for (String sub_item : item.split("\n")) {
                for (String line : sub_item.split("(?<=\\G.{" + (MAX_WIDTH_CHAR - 3) + "})")) {
                    // - 3 because the start of the line is 3 wide
                    result.add(current_line + line);
                    current_line = " &7  ";
                }
            }
        }
        if (lore.getAction_left() == null || lore.getAction_right() == null) {
            if (lore.getAction_right() == null && lore.getAction_left() == null) {
                // If none of them are present, don't add an extra line
                if (lore.getAction_drop() != null) {
                    if (result.size() != 0) {
                        // Something added so far - need to add extra line
                        result.add("");
                    }
                    result.add(" &bQ &7" + lore.getAction_drop());
                }
                if (lore.getFooter() != null) {
                    if (result.size() != 0) {
                        // Something added so far - need to add extra line
                        result.add("");
                    }
                    result.add("&8" + lore.getFooter());
                }
                return result;
            }
            // Add extra line, one of them is present
            result.add("");
            if (lore.getAction_left() == null) {
                result.add(" &c- &7" + lore.getAction_right());
            } else {
                result.add(" &a+ &7" + lore.getAction_left());
            }
        } else {
            if (lore.getAction_left().length() + lore.getAction_right().length() + 7 <= MAX_WIDTH_CHAR) {
                // + 7 because " + " & "| - " use 7 characters width; if smaller -> draw on one line
                result.add("");
                result.add(" &a+ &7" + lore.getAction_left() + " &f| &c- &7" + lore.getAction_right());
            } else {
                // Each reaction gets its own line
                result.add("");
                result.add(" &a+ &7" + lore.getAction_left());
                result.add(" &c- &7" + lore.getAction_right());
            }
        }
        if (lore.getAction_drop() != null) {
            result.add(" &bQ &7" + lore.getAction_drop());
        }
        if (lore.getFooter() != null) {
            result.add("");
            result.add("&8" + lore.getFooter());
        }
        return result;
    }
}
