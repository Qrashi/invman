package link.ignyte.plugins.invman.item;

import link.ignyte.plugins.invman.item.event.ItemEvent;

import java.util.function.Consumer;

/**
 * A class describing what to do on item click
 */
public class InteractionManager {
    protected Consumer<ItemEvent> runnable;
    public boolean runnable_enabled;

    /**
     * Create a new InteractionManager
     */
    public InteractionManager() {
        this.runnable = (ItemEvent event) -> event.cancel = true;
        runnable_enabled = true;
    }

}
