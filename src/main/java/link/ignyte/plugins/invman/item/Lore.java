package link.ignyte.plugins.invman.item;


import link.ignyte.plugins.invman.Invman;
import static link.ignyte.plugins.invman.utils.Color.translate;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * The Lore (description text) of an Item
 */
@Getter
public class Lore {
    @Getter(AccessLevel.NONE)
    boolean updated;
    @Getter(AccessLevel.NONE)
    Item base;
    String title = null;
    ArrayList<String> description;
    String footer = null;
    String action_right = null;
    String action_left = null;
    String action_drop = null;

    /**
     * Initialize a lore from an already existing lore (List)
     * @param base base item
     */
    public Lore(Item base, List<String> baseLore) {
        this.base = base;
        if (baseLore == null) {
            description = new ArrayList<>();
        } else {
            description = new ArrayList<>(baseLore);
        }
    }

    /**
     * Initialize a "new" lore from a default item
     * @param base base item
     */
    public Lore(Item base) {
        this.base = base;
        description = new ArrayList<>();
    }

    /**
     * Apply the lore
     * @return return self (singleton pattern)
     */
    public Item apply() {
        if (updated) {
            base.metaChanged = true;
            base.meta.setLore(translate(Invman.lore_serializer.serialize(this)));
        }
        return base;
    }

    /**
     * Edit the title
     * @param title new title (null for no title)
     * @return return self (singleton pattern)
     */
    public Lore setTitle(String title) {
        updated = updated || !title.equals(this.title); // updated is true if already true or title not same as previous
        this.title = title;
        return this;
    }

    /**
     * Set description, will always force a "reserialisation" of the lore
     * @param description new description
     * @return return self (singleton pattern)
     */
    public Lore setDescription(ArrayList<String> description) {
        updated = true;
        this.description = description;
        return this;
    }

    /**
     * Edit a line in the description
     * @param line line number
     * @param content new content
     * @return return self (singleton pattern)
     */
    public Lore editLine(int line, String content) {
        updated = updated || !description.get(line).equals(content);
        description.set(line, content);
        return this;
    }

    /**
     * Add a new line to the description
     * @param content content to add
     * @return return self (singleton pattern)
     */
    public Lore addLine(String content) {
        updated = true;
        description.add(content);
        return this;
    }

    /**
     * Edit the footer, null for no footer
     * @param footer new footer
     * @return return self (singleton pattern)
     */
    public Lore setFooter(String footer) {
        updated = updated || !footer.equals(this.footer);
        this.footer = footer;
        return this;
    }

    /**
     * Edit the right click action, null for no action
     * @param action_right new right click action
     * @return return self (singleton pattern)
     */
    public Lore setAction_right(String action_right) {
        updated = updated || !action_right.equals(this.action_right);
        this.action_right = action_right;
        return this;
    }

    /**
     * Change the left click action, null for no action
     * @param action_left new left click action
     * @return return self (singleton pattern)
     */
    public Lore setAction_left(String action_left) {
        updated = updated || !action_left.equals(this.action_left);
        this.action_left = action_left;
        return this;
    }

    /**
     * Change the action on drop, null for no action
     * @param action_drop new drop action
     * @return return self (singleton pattern)
     */
    public Lore setAction_drop(String action_drop) {
        updated = updated || !action_drop.equals(this.action_drop);
        this.action_drop = action_drop;
        return this;
    }
}
