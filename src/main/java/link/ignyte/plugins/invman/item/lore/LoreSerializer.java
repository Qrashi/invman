package link.ignyte.plugins.invman.item.lore;

import link.ignyte.plugins.invman.item.Lore;

import java.util.ArrayList;

/**
 * A lore editor
 * by defining how to edit / create a lore you can specify the general formatting of a lore
 * Important: every empty field (e.g action_left) is considered as nonexistent, please deal accordingly
 */
public interface LoreSerializer {
    /**
     * Serialize a lore object to an array of text (use color codes!)
     * @param lore lore object
     * @return text array
     */
    ArrayList<String> serialize(Lore lore);
}
