package link.ignyte.plugins.invman;

import link.ignyte.plugins.invman.item.Item;
import link.ignyte.plugins.invman.item.listeners.ItemListeners;
import link.ignyte.plugins.invman.item.listeners.ListenerStatistics;
import link.ignyte.plugins.invman.item.lore.DefaultLayoutSerializer;
import link.ignyte.plugins.invman.item.lore.LoreSerializer;
import link.ignyte.utils.JsonSingleton.GsonMode;
import link.ignyte.utils.JsonSingleton.JsonSingletons;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

/**
 * InvMan - the inventory management library
 */
public final class Invman {

    private static boolean initialized;
    public static LoreSerializer lore_serializer = new DefaultLayoutSerializer();
    private static JavaPlugin plugin;
    public static Item filler = null;
    private static UUID instanceUUID = UUID.randomUUID();

    /**
     * Initialize InvMan
     * @param plugin plugin to register to
     */
    public static void initialize(JavaPlugin plugin) {
        if (initialized) {
            return;
        }
        Invman.plugin = plugin;
        JsonSingletons.initialize(GsonMode.ALL, plugin.getDataFolder() + "/statistics");
        plugin.getServer().getPluginManager().registerEvents(new ItemListeners(), plugin);
        initialized = true;
        filler = new Item(Material.GRAY_STAINED_GLASS_PANE).setName("").makeUnmoveable().apply();
    }

    /**
     * Throw an error if the plugin has not been initialized
     * @param error the error description (if uninitialized)
     */
    public static void throwUninitialized(String error) {
        if (!initialized) {
            throw new NotInitializedException(error);
        }
    }

    /**
     * Get the JavaPlugin tied to this instance
     * @return the java plugin
     */
    public static JavaPlugin getPlugin() {
        return plugin;
    }

    /**
     * Get the InstanceUUID tied to this InvMan instance
     * @return InstanceUUID
     */
    public static UUID getInstanceUUID() {
        return instanceUUID;
    }

    /**
     * Disable InvMan (save statistics)
     */
    public void disable() {
        ListenerStatistics.save(ListenerStatistics.class);
    }

    /**
     * Reload invman, remove all items in database and invalidate old ones
     */
    public void reload() {
        instanceUUID = UUID.randomUUID();
    }

}
