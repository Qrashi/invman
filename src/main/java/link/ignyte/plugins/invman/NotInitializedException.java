package link.ignyte.plugins.invman;

/**
 * An exception stating that InvMan has not been initialized properly
 */
public class NotInitializedException extends RuntimeException {

    /**
     * New NotInitializedException, called when InvMan has not been initialized properly
     * @param s exception description
     */
    NotInitializedException(String s) {
        super(s);
    }
}
